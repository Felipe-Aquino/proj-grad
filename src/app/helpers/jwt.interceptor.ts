import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { TranslationService } from './translation.service';

import { notify } from '../modules/ui/ui-toast.component';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  expirationMsg: string;

  constructor(private router: Router, private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);
    this.expirationMsg = tr('interceptor-expired-msg');
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    let token = localStorage.getItem('token');

    if(token) {
      request = request.clone({
        setHeaders: {
          "x-access-token": token,
          "authorization": token
        }
      });
    }

    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want
          //console.log('HttpResponse');
        }
      },
      (err: any) => {
        //console.log('intercept err', err);
        if (err instanceof HttpErrorResponse) { // intercepting token expiration
          if (err.status === 401) {
            this.router.navigate(['/login']);
            notify(this.expirationMsg, 'danger', 4000);
          }
        }
      })
    )
  }
}
