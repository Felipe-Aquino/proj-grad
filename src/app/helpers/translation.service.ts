import { Injectable } from '@angular/core';
import * as pt from '../../assets/langs/pt.json';
import * as es from '../../assets/langs/es.json';
import * as en from '../../assets/langs/en.json';

interface TranslationSet {
   language: string;
   values: {[key: string]: string};
}

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  private language: string = 'en';

  private dictionary: {[key: string]: TranslationSet} = {
    'pt' : (<any>pt).default as TranslationSet,
    'es' : (<any>es).default as TranslationSet,
    'en' : (<any>en).default as TranslationSet
  };

  constructor() {
    const navLang = navigator.language;
    const lang = localStorage.getItem('language');
    this.language = 'en';

    if (lang) {
      this.language = lang;
    } else if (navLang) {
      this.language = navLang.substr(0, 2);
      if (['en', 'es', 'pt'].includes(this.language) == false) {
        this.language = 'en';
      }
    }
  }

  locale(language: string) {
    if (language in this.dictionary) {
      this.language = language;
      localStorage.setItem('language', language);
    }
  }

  getLanguage() {
    return this.language;
  }

  translate(value: string): string {
    if (this.dictionary[this.language]) {
       return this.dictionary[this.language].values[value];
    }
    return '';
  }
}
