import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { ApiUrls } from '../../../config';

@Injectable()
export class AuthService {

  constructor(private router: Router, private http: HttpClient) {
  }

  login(email: string, password: string) {
    return this.http.post<any>(ApiUrls.auth, { email, password }).pipe(
      map((response: any) => {
        // login successful if there's a jwt token in the response
        if(response.token) {
          // store jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', response.token);
        }

        return response;
      })
    );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
}
