import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { TranslationService } from '../../../helpers/translation.service';

@Component({
  selector: 'app-login-home',
  templateUrl: './login-home.component.html',
  styleUrls: ['./login-home.component.css']
})
export class LoginHomeComponent implements OnInit {
  email: string = '';
  password: string = '';
  returnUrl: string = '';
  loginError: boolean = false;
  selectedLang: string = '';

  loading: boolean = false;

  translations: any;

  languages: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: AuthService,
    private translationService: TranslationService,
    private zone: NgZone
  ) {
    let tr = this.translationService.translate.bind(this.translationService);

    // reset login
    this.service.logout();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'];
    this.loginError = false;

    this.translations = {
      your_email: tr('general-your-email'),
      your_pw:    tr('general-your-password'),
      remember:   tr('login-remember'),
      login:      tr('general-login'),
      signup:     tr('general-sign-up'),
      please:     tr('login-please'),
      forgot:     tr('login-forgot'),
      help:       tr('login-help'),
      wrong:      tr('login-wrong'),
    };

    this.languages = [
      { value: 'en', name: tr('language-en') },
      { value: 'es', name: tr('language-es') },
      { value: 'pt', name: tr('language-pt') },
    ];

    this.selectedLang = this.translationService.getLanguage();
  }

  ngOnInit() {
    this.email    = '';
    this.password = '';
    this.loading = false;
    // this.adminLogin();
  }

  doLogin() {
    this.loading = true;
    this.service.login(this.email, this.password)
      .subscribe(data => {
        this.loginError = false;

        if (!this.returnUrl) {
          this.returnUrl = data.admin ? '/admin' : '/user' ;
        }

        this.loading = false;
        this.router.navigate([this.returnUrl]);
      }, error => {
        this.loading = false;
        this.loginError = true;
        this.email    = '';
        this.password = '';
      });
   }

  adminLogin() {
    this.email = 'admin@admin.com';
    this.password = '123456';
  }

  userLogin() {
    this.email = 'felipe@felipe.com';
    this.password = 'felipe';
  }

  langChanged(value: string) {
    console.log('changed', value, this.selectedLang, this.translationService.getLanguage());
    if (value != this.translationService.getLanguage()) {
      this.translationService.locale(value);
      this.zone.run(() => document.location.reload());
    }
  }
}
