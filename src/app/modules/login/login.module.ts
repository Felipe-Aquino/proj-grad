import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AuthService } from './services/auth.service';

import { LoginRoutingModule } from './login-routing.module';
import { LoginHomeComponent } from './login-home/login-home.component';
import { SharedModule } from '../ui/ui-shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [LoginHomeComponent],
  exports: [
    LoginHomeComponent,
  ],
  providers: [
    AuthService,
  ],
})
export class LoginModule { }
