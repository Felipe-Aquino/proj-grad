import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TranslationService } from '../../helpers/translation.service';

@Component({
  selector: 'ui-sidebar',
  template: `
    <div style="position: fixed; height: 100%">
      <aside class="menu">
        <p class="menu-label">
          {{translations.general}}
        </p>
        <ul class="menu-list">
          <li *ngFor="let item of items; let i = index">
            <a [class.is-active]="selected == i" (click)="select(i)">
              {{item}}
            </a>
          </li>
        </ul>
      </aside>
    </div>
  `,
  styleUrls: ['./ui.css']
})
export class UISidebarComponent {
  @Input() items: string[] = [];
  @Output() onSelect: EventEmitter<number> = new EventEmitter();

  selected: number;
  translations: any;

  constructor(private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.selected = 0;

    this.translations = {
      general: tr('general-general'),
    };
  }

  select(key: number) {
    this.selected = key;
    this.onSelect.emit(key);
  }

}
