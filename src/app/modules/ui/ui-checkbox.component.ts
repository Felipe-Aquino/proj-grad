import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-checkbox',
  template: `
    <div>
      <div class="ui-chkbox">
        <div class="ui-helper">
          <input 
            [checked]="value"
            [disabled]="disabled"
            type="checkbox">

        </div>
        <div [ngClass]="{'ui-chkbox-box': true, 'nice-blue-bg': value}" (click)="changed()">
          <span [ngClass]="{'ui-chkbox-icon': true, 'checked': value}"></span>
        </div>
      </div>
      <label class="ui-chkbox-label"> {{text}} </label>
    </div>
  `,
  styles: [`
    input[type="checkbox" i] {
      background-color: initial;
      cursor: default;
      margin: 3px 0.5ex;
      padding: initial;
      border: initial;
    }

    .ui-chkbox {
      display: inline-block;
      vertical-align: middle;
      margin: 0;
      width: 20px;
      height: 20px;
      box-sizing: border-box;
      cursor: pointer;
      -webkit-user-select: none;

      font-family: "Open Sans", "Helvetica Neue", sans-serif;
      font-size: 14px;
      text-decoration: none;
    }


    .ui-helper {
      border: 0;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;

      box-sizing: border-box;
    }

    .ui-chkbox-box {
        line-height: 1.125em;
        background-color: white;
        border: 1px solid transparent;
        border-color: #dbdbdb;
        color: #363636;
        box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
        width: 20px;
        height: 20px;
        text-align: center;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        -moz-transition: background-color 0.2s, border-color 0.2s, box-shadow 0.2s;
        -o-transition: background-color 0.2s, border-color 0.2s, box-shadow 0.2s;
        -webkit-transition: background-color 0.2s, border-color 0.2s, box-shadow 0.2s;
        transition: background-color 0.2s, border-color 0.2s, box-shadow 0.2s;
    }

    .ui-chkbox-box:hover {
        border-color: #b5b5b5;

    }

    .checked {
      left: 7px;
      top: 3px;
      width: 5px;
      height: 10px;
      border: solid white;
      border-width: 0 3px 3px 0;
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
    }

    .nice-blue-bg {
      background-color: #2196F3;
    }

    .ui-chkbox-icon {
      overflow: hidden;
      position: relative;
      font-size: 18px;
      display: block;
      box-sizing: border-box;
    }

    .ui-chkbox-label {
      margin: 0 0 0 0.5em;
      vertical-align: middle;
      box-sizing: border-box;
      cursor: default;
    }

  `]
})
export class UICheckboxComponent { 
  @Input() text: string = '';
  @Input() disabled: boolean = false;

  @Input() value: boolean = false;
  @Output() valueChange = new EventEmitter<boolean>();

  checkClass: any = {};

  constructor() {
  }

  changed() {
    this.value = !this.value;
    console.log('changing', this.value);
  }
}
