import { Component, Input, Output, EventEmitter } from '@angular/core';
import { EventEmitterService } from "../../event-emitter.service";

type ToastType = 'info' | 'danger' | 'success' | 'warn';

class Message {
  content: string;
  ttype: ToastType;

  constructor(content: string, ttype?: ToastType) {
    this.content = content;
    this.ttype = ttype || 'warn';
  }
}

@Component({
  selector: 'ui-toast',
  template: `
  <div class="wrapper" *ngIf="message != null">
    <div class="notification"
        [ngClass]="{'is-info':     message.ttype=='info',
                    'is-warning':     message.ttype=='warn',
                    'is-danger':   message.ttype=='danger',
                    'is-success':  message.ttype=='success'}">

      <button class="delete" (click)="dismiss()"></button>
      {{message.content}}
    </div>
  </div>
  `,
  styleUrls: ['./ui.css']
})
export class ToastComponent {
  message: Message | null;
  defaultTimeout: number;

  constructor() {
    this.message = null;
    this.defaultTimeout = 3000;

    //console.log('created')
    EventEmitterService.get('toast').subscribe(data => this.notify(data.content, data.ttype, data.timeout));
  }

  notify(content: string, ttype?: ToastType, timeout?: number) {
    this.message = new Message(content, ttype);
    setTimeout(() => this.dismiss(), timeout || this.defaultTimeout);
    //console.log('notified')
  }

  dismiss() {
    this.message = null;

    //console.log('dismissed')
  }

}

export function notify(content: string, ttype?: ToastType, timeout?: number) {
    EventEmitterService.get('toast').emit({content, ttype, timeout});
}
