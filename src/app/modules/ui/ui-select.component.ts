import {
  Component,
  OnInit, AfterViewInit,
  ContentChildren, ViewChild,
  QueryList,
  Output, Input, EventEmitter
} from '@angular/core';

@Component({
  selector: 'ui-select',
  template: `
    <div [ngClass]="_cssClass">
      <select [(ngModel)]="selected" (change)="onChange()">
        <option *ngFor="let data of _dataSource" 
                [value]="data.value">
          {{data.name}}
        </option>
      </select>
    </div>
  `,
  styleUrls: ['./ui.css']
})
export class UISelectComponent {
  @Output() onChanged = new EventEmitter<string>();

  @Input() 
  set dataSource(data: DataSource) {
    this._dataSource = data;
  }

  @Input()  selected!: string;
  @Output() selectedChange = new EventEmitter<string>();

  @Input()
  set cssClass(cssClass: any) {
    if (cssClass) {
      this._cssClass = {
        'select': true,
        ...cssClass
      };
    }
  }

  _dataSource: DataSource;
  _cssClass: any;

  constructor() {
    this._dataSource = [];
    this._cssClass = {'select': true};
  }

  onChange() {
    this.onChanged.emit(this.selected);
    this.selectedChange.emit(this.selected);
  }
}

export type DataSource = Array<{
  name: string,
  value: string
}>;
