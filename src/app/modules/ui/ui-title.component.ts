import { Component } from '@angular/core';

@Component({
  selector: 'ui-title',
  template: `
    <section class="hero is-info welcome is-small">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">
            <ng-content select=".ui-titled"></ng-content>
          </h1>
          <h2 class="subtitle">
            <ng-content select=".ui-subtitled"></ng-content>
          </h2>
        </div>
      </div>
    </section>
  `,
  styleUrls: ['./ui.css']
})
export class UITitleComponent { }
