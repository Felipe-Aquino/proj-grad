import {
  Component,
  OnInit, AfterViewInit,
  ContentChildren, ViewChild,
  QueryList,
  Output, Input, EventEmitter
} from '@angular/core';

@Component({
  selector: 'ui-select-item',
  template: `
    <div class="dropdown-item">
      <label class="container container-check checkbox">
        <input type="checkbox" [(ngModel)]="isChecked" (change)="onChange()">
        <span class="checkmark"></span>
        <ng-content></ng-content>
      </label>
    </div>
  `,
  styleUrls: ['./ui.css']
})
export class UISelectItem {
  @Output() onChanged = new EventEmitter<boolean>();

  isChecked: boolean;

  constructor() {
    this.isChecked = false;
  }

  onChange() {
    this.onChanged.emit(this.isChecked);
  }
}

@Component({
  selector: 'ui-multiselect',
  template: `
    <div [ngClass]="{'dropdown': true, 'is-active': activate}">
      <div class="dropdown-trigger">
        <button class="button" aria-haspopup="true" (click)="onActivate()">
          <span>{{selectBtn}}</span>
          <span class="icon is-small">
            <i class="fas fa-angle-down"></i>
          </span>
        </button>
      </div>
      <div class="dropdown-menu" id="dropdown-menu">
        <div class="dropdown-content" style="display: flex; flex-direction: column;">
          <ui-select-item (onChanged)="selectAll($event)"> {{selectAllMsg}} </ui-select-item>

          <hr class="dropdown-divider">
 
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./ui.css']
})
export class UIMultiSelectComponent implements OnInit, AfterViewInit {
  @ContentChildren(UISelectItem) items!: QueryList<UISelectItem>;
  @ViewChild(UISelectItem) selectAllItem!: UISelectItem;

  @Output() onChanged = new EventEmitter<boolean[]>();

  @Input() selectAllMsg: string = '';
  @Input() selectBtn: string = '';

  activate: boolean;
  status: boolean[];

  constructor() {
    this.activate = false;
    this.status = [];
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.items.forEach((item, index) => {
      this.status.push(item.isChecked);
      item.onChanged.subscribe((event: boolean) => {
        this.status[index] = event;
        this.onChanged.emit(this.status);

        this.selectAllItem.isChecked = false;
        for(const s of this.status) {
          if(!s) return;
        }
        this.selectAllItem.isChecked = true;
      });
    });
  }

  onActivate() {
    this.activate = !this.activate;
  }

  selectAll(event: boolean) {
    this.items.forEach((item, i) => {
      item.isChecked = event;
      this.status[i] = event;
    });
    this.onChanged.emit(this.status);
  }
}
