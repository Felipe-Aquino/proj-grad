import { 
  Component, 
  Input, 
  Output, 
  EventEmitter,
  TemplateRef,
  ContentChildren,
  QueryList,
  AfterViewInit
} from '@angular/core';

@Component({
  selector: 'ui-column',
  template: ``,
  styles: []
})
export class UIColumnComponent { 
  @Input() caption: string = '';

  /* Devextreme-like params*/
  @Input() valueExpr: string = '';
  @Input() displayExpr: string = '';

  @Input() uiTemplate!: TemplateRef<any>;

  public _cssStyle: any;
  @Input() set cssStyle(style: any) {
    if (style) this._cssStyle = style;
  }

  constructor() {
    this._cssStyle = {};
  }
}

@Component({
  selector: 'ui-table',
  template: `
    <ng-template #defaultTemplate let-data="data">
       {{formatData(data.row[data.col.displayExpr])}}
    </ng-template>

    <table class="table is-fullwidth is-striped"> 
      <thead>
        <tr>
          <th *ngFor="let col of columns" [ngStyle]="col._cssStyle">
            {{col.caption}}
          </th>
        </tr>
      </thead>
      <tbody [ngStyle]="_bodyStyle">
        <tr *ngFor="let row of rows">
          <td *ngFor="let col of columns" [ngStyle]="col._cssStyle">
            <ng-template 
              [ngTemplateOutlet]="col.uiTemplate || defaultTemplate"
              [ngTemplateOutletContext]="{data: {row: row, col: col}}">
            </ng-template>
          </td>
        </tr>
      </tbody>
      
      <ng-content></ng-content>

    </table> 
  `,
  styles: [`
    tbody {
      display: block;
      max-height: 300px;
      overflow: auto;
    }

    thead, tbody tr {
      display: table;
      width: 100%;
      table-layout: fixed;/* even columns width , fix width of table too*/
    }
  `]
})
export class UITableComponent implements AfterViewInit { 
  @ContentChildren(UIColumnComponent) columns!: QueryList<UIColumnComponent>;

  rows: Array<any>;
  @Input() set dataSource(data: Array<any>) {
    this.rows = data || [];
    console.log('rows', data);
  }

  _bodyStyle: any;
  @Input()
  set bodyStyle(style: any) {
    if (style) this._bodyStyle = style;
  }
  
  constructor() {
    this.rows = [];
    this._bodyStyle = {};
  }

  ngAfterViewInit() {
    this.columns.forEach((column, index) => {
    });
  }

  formatData(data: any): string {
    if (data == null)
      return ' ';
    if (data instanceof Date)
      return data.toLocaleDateString();
    if (!isNumber(data)) {
      if (Number.isInteger(data))
        return data;
      return data.toFixed(2);
    }
    return data
  }
}

function isNumber(data: any): boolean {
  return !Number.isNaN(data);
}

