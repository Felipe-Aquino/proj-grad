import {
  Component,
  EventEmitter,
  Input, Output, OnInit
} from '@angular/core';

type ColorType = 'is-primary' | 'is-link'
               | 'is-info'    | 'is-success'
               | 'is-warning' | 'is-danger'
               | 'is-white'   | 'is-light'
               | 'is-dark'    | 'is-black';

type StylingMode = 'is-none' | 'is-text' | 'is-outlined';
type SizeType = 'is-small' | 'is-normal' | 'is-medium' | 'is-large';

@Component({
  selector: 'ui-button',
  template: `
    <button [title]="title"
      [disabled]="disabled"
      [ngClass]="classes"
      (click)="clicked()">
      <span [ngClass]="iconClasses" *ngIf="icon != null">
        <i [ngClass]="['fas', icon]"></i>
      </span>
      <span *ngIf="text != null"> {{text}} </span>
    </button>
  `,
  styles: []
})
export class UIButtonComponent implements OnInit {
  @Output() onClick = new EventEmitter<boolean>();

  @Input() loading: boolean = false;
  @Input() disabled: boolean = false;
  @Input() text: string = '';
  @Input() title: string = '';
  @Input() icon: string = '';

  _type: ColorType = 'is-primary';

  @Input()
  set type(t: ColorType) {
    this._type = t;
  }

  iconSizes: any;

  _size: SizeType = 'is-small';
  @Input()
  set size(size: SizeType) {
    this._size = size;
  }
  
  _stylingMode: StylingMode = 'is-none';
  @Input() 
  set stylingMode(styling: StylingMode) {
    this._stylingMode = styling;
  }

  classes: any;
  iconClasses: any;

  constructor() {
    this.iconSizes = {
      'is-small': 'sm',
      'is-normal': 'lg',
      'is-medium': '2x',
      'is-large': '3x'
    };
  }

  ngOnInit() {
    this.classes = {
      button: true, 
      [this._size]: true,
      [this._type]: true, 
      [this._stylingMode]: true,
      'is-loading': this.loading ? true : false
    }

    this.iconClasses = {'icon': true, [this._size]: true};
  }

  clicked() {
    this.onClick.emit(true);
  }
}
