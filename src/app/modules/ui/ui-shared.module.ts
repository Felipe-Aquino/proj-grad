import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UIButtonComponent } from './ui-button.component';
import { UICheckboxComponent } from './ui-checkbox.component';
import { UISwitchComponent } from './ui-switch.component';
import { UISidebarComponent } from './ui-sidebar.component';
import { UITitleComponent } from './ui-title.component';
import { UIMultiSelectComponent, UISelectItem } from './ui-multiselect.component';
import { UISelectComponent } from './ui-select.component';
import { UITableComponent, UIColumnComponent } from './ui-table.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    UISwitchComponent,
    UICheckboxComponent,
    UISidebarComponent,
    UITitleComponent,
    UISelectItem,
    UIMultiSelectComponent,
    UISelectComponent,
    UIButtonComponent,
    UITableComponent,
    UIColumnComponent,
  ],
  exports: [
    UISwitchComponent,
    UICheckboxComponent,
    UISidebarComponent,
    UITitleComponent,
    UISelectItem,
    UIMultiSelectComponent,
    UISelectComponent,
    UIButtonComponent,
    UITableComponent,
    UIColumnComponent,
  ]
})
export class SharedModule { }
