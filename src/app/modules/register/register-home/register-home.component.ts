import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../services/register.service';
import { TranslationService } from '../../../helpers/translation.service';

import User from '../../user/models/user.model';

import { notify } from '../../ui/ui-toast.component';

@Component({
  selector: 'app-register-home',
  templateUrl: './register-home.component.html',
  styleUrls: ['./register-home.component.css']
})
export class RegisterHomeComponent implements OnInit {
  registrationForm: FormGroup;
  passwordForm: FormGroup;

  loading: boolean = false;
  translations: any;

  constructor(
    private router: Router,
    private service: RegisterService,
    private translationService: TranslationService,
    private builder: FormBuilder
  ) {
    this.passwordForm = this.builder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validator: RegistrationValidator.validate.bind(this)
    });

    this.registrationForm = this.builder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      passwordForm: this.passwordForm
    });

    let tr = this.translationService.translate.bind(this.translationService);

    this.translations = {
      your_name:        tr("register-your-name"),
      your_email:       tr("general-your-email"),
      your_pw:          tr("general-your-password"),
      confirm:          tr("register-confirm-password"),
      required_name:    tr("register-required-name"),
      required_email:   tr("register-required-name"),
      required_pw:      tr("register-required-password"),
      required_confirm: tr("register-required-confirm"),
      pws_not_match:    tr("register-password-match"),
      signup:           tr("general-sign-up"),
      register:         tr("register-proceed"),
      back:             tr("register-back-login"),
      notify_success:   tr("register-notify-success"),
      notify_failure:   tr("register-notify-failure")
    };
  }

  ngOnInit() {
    this.loading = false;
  }

  doRegister() {
    const name = this.registrationForm.controls.name.value;
    const email = this.registrationForm.controls.email.value;
    const password = this.passwordForm.controls.password.value;
    const user = new User(name, email, password, false);

    this.loading = true;
    this.service.registerUser(user).subscribe(
      res => {
        console.log(res);
        notify(this.translations.notify_success, 'success');
        this.loading = false;
        this.router.navigate(['']);
      },
      err => {
        this.loading = false;
        notify(this.translations.notify_failure, 'danger');
      }
    );
  }
}

class RegistrationValidator {
  static validate(registrationForm: FormGroup) {
    let password = registrationForm.controls.password.value;
    let repeatPassword = registrationForm.controls.confirmPassword.value;

    if (repeatPassword.length <= 0) {
      return null;
    }

    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }

    return null;

  }
}
