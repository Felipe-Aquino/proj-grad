import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RegisterService } from './services/register.service';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterHomeComponent } from './register-home/register-home.component';

@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [RegisterHomeComponent],
  exports: [
    RegisterHomeComponent,
  ],
  providers: [
    RegisterService
  ]
})
export class RegisterModule { }
