import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

import { ApiUrls } from '../../../config';
import User from '../../user/models/user.model';

@Injectable()
export class RegisterService {

  constructor(private http: HttpClient) {
  }

  registerUser(user: User): Observable<any> {
    return this.http.post(ApiUrls.register, user);
  }
}
