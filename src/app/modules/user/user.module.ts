import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FileUploadModule } from 'ng2-file-upload';

import { UserService } from './services/user.service';
import { AuthService } from '../login/services/auth.service';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { UserAddDocComponent } from './user-add-doc/user-add-doc.component';
import { UserDocInfoComponent } from './user-doc-info/user-doc-info.component';
import { UserDocListComponent } from './user-doc-list/user-doc-list.component';

import { SharedModule } from '../ui/ui-shared.module';

import { TagCloudModule } from 'angular-tag-cloud-module';
import { NotificationComponent } from './notification/notification.component';
import { ReportComponent } from './report/report.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    UserRoutingModule,
    FileUploadModule,
    SharedModule,
    TagCloudModule,
  ],
  declarations: [
    UserComponent,
    UserHomeComponent,
    UserAddDocComponent,
    UserDocInfoComponent,
    UserDocListComponent,
    NotificationComponent,
    ReportComponent
  ],
  exports: [
    UserHomeComponent,
  ],
  providers: [
    UserService,
    AuthService
  ]
})
export class UserModule { }
