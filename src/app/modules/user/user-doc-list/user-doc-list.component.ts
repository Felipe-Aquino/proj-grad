import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { File } from '../models/file.model';
import { TranslationService } from '../../../helpers/translation.service';

@Component({
  selector: 'app-user-doc-list',
  templateUrl: './user-doc-list.component.html',
  styleUrls: ['./user-doc-list.component.css']
})
export class UserDocListComponent implements OnInit {

  @Input() files: File[] = [];
  @Output() onSelect = new EventEmitter<File>();
  @Output() onDelete = new EventEmitter<string>();
  @Output() onUpdate = new EventEmitter<File>();

  translations: any;

  constructor(private translationService: TranslationService) {

    let tr = this.translationService.translate.bind(this.translationService);

    this.translations = {
      name: tr('general-name'),
      date: tr('general-date'),
      pub:  tr('user-public'),
      show: tr('general-show'),
      del:  tr('general-delete'),
      your_docs:  tr('user-your-docs'),
      none_found: tr('user-no-file-found'),
    };
  }

  ngOnInit() {
  }

  selectFile(id: string) {
    let file = this.files.find((value, index, obj) => value.id == id)
    this.onSelect.emit(file);
  }

  deleteFile(id: string) {
    this.onDelete.emit(id);
  }

  update(file: File) {
    this.onUpdate.emit(file);
  }
}
