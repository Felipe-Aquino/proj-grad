import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDocListComponent } from './user-doc-list.component';

describe('UserDocListComponent', () => {
  let component: UserDocListComponent;
  let fixture: ComponentFixture<UserDocListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDocListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDocListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
