import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { UserService } from '../services/user.service';
import { File, Keyword } from '../models/file.model';
import { TranslationService } from '../../../helpers/translation.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  @Output() onSelect: EventEmitter<File> = new EventEmitter<File>()

  allFiles: File[] = [];
  searching: boolean = false;
  cardMessage: string = '';
  cardFiles: File[] = [];

  searchTypes!: any;
  selectedType!: string;

  valueSearched: string;
  searchSystem: boolean;

  categories: any = [];

  translations: any;

  constructor(private service: UserService, private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.searching = false;

    this.searchTypes = [
      {value: 'N', name: tr("user-search-type-name")},
      {value: 'C', name: tr("user-search-type-class")},
      {value: 'K', name: tr("user-search-type-keyword")}
    ]

    this.selectedType = this.searchTypes[0].value;

    this.valueSearched = "";
    this.searchSystem = false;

    this.translations = {
      by:     tr("general-by"),
      name:   tr("general-name"),
      show:   tr("general-show"),
      submit: tr("general-submit"),

      search_for:   tr("user-search-for"),
      search_files: tr("user-search-files"),
      search_sys:   tr("user-search-system"),
      doc_search:   tr("user-doc-search"),
      card_msg1:    tr("user-card-msg1"),
      card_msg2:    tr("user-card-msg2"),
      none_fits:    tr("user-no-file-fits"),
      none_found:   tr("user-no-file-found"),
    };

    this.cardMessage = this.translations.card_msg1;
  }

  ngOnInit() {
  }

  selectFile(id: string) {
    let file = this.cardFiles.find((value, index, obj) => value.id == id)
    this.onSelect.emit(file);
  }

  @Input()
  set files(files: File[]) {
    if (files != null) {
      this.allFiles = files;
      this.cardFiles = files.slice(0, 5);

      let categs: any = {};
      let keywords: any[] = [];

      for (let file of files) {
        keywords.push(
          ...file.keywords,
          ...file.given_keywords
        );
      }

      for (let keyword of keywords) {
        if (keyword.classifications) {
          keyword.classifications.forEach((c: any) => {
           const label = c.label as string;
           categs[label] = categs[label] ? categs[label] + 1 : 1;
          });
        }
      }

      for (let c in categs) {
        const key = c as string;
        this.categories.push({
          key,
          value: categs[key],
        });
      }

      this.categories.sort((a: any, b: any) => b.value - a.value);
      this.categories = this.categories.splice(0, 20);

    } else {
      this.allFiles = [];
      this.cardFiles = [];
    }
  }

  onSearch() {
    this.searching = true;
    this.cardFiles = [];
    if(!this.searchSystem) {
      this.searchUserFiles();
    } else {
      this.service.searchFiles(this.valueSearched, this.selectedType).subscribe(
        files => { this.cardFiles = files; console.log('files', files); }
      )
    }

    console.log('file', this.cardFiles)
    this.cardMessage = this.translations.card_msg2;
  }

  searchUserFiles() {
    switch(this.selectedType) {
      case 'N':
        this.cardFiles = this.allFiles.filter(v => nameMatch(v.name, this.valueSearched));
        break;
      case 'K':
        this.cardFiles = this.allFiles.filter((value, index, obj) => {
          let r = value.keywords.findIndex(v => nameMatch(v.word, this.valueSearched));
          if (r != -1) return true;
          r = value.given_keywords.findIndex(v => nameMatch(v.word, this.valueSearched));
          if (r != -1) return true;
          return false;
        });
        break;
      case 'C':
        this.cardFiles = this.allFiles.filter(v => {
          return false;
          //@Refactor: Redo the search by classification
          //return matchName(v.classification, this.valueSearched);
        });
        break;
    }
  }
}

function nameMatch(name: string, searched: string) {
  return name.search(new RegExp(searched, 'i')) != -1;
}
