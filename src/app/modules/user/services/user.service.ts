import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map, mergeMap, finalize, concatMap } from 'rxjs/operators';
import { from, pipe } from 'rxjs';

import User from '../models/user.model';
import { File, FileInfo } from '../models/file.model';
import { Notification } from '../models/notification.model';
import { ApiUrls } from '../../../config';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
    /*setInterval(() => {
      console.log('still alive'); 
    });*/
  }

  createUser(user: User): Observable<any> {
    return this.http.post(ApiUrls.register, user);
  }

  getUsers(): Observable<any> {
    return this.http.get(ApiUrls.user);
  }

  editUser(user: User): any {
    return this.http.put(ApiUrls.user, user);
  }

  deleteUser(): any {
    return this.http.delete(ApiUrls.user);
  }

  createFile(file: FileInfo): Observable<any> {
    return this.http.post(ApiUrls.file, file);
  }

  getFiles(): Observable<any> {
    return this.http.get(ApiUrls.file).pipe(
      map((res: any) => res.data as File[])
    );
  }

  searchFiles(value: string, selectType: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('value', value);
    params = params.append('type', selectType);
    return this.http.get(ApiUrls.file_search, { params }).pipe(
      map((res: any) => res.data as File[])
    );
  }

  editFile(file: File): any {
    return this.http.put(`${ApiUrls.file}/${file.id}`, file);
  }

  deleteFile(id: string): any {
    return this.http.delete(`${ApiUrls.file}/${id}`);
  }

  getNotifications(): Observable<Notification[]> {
    return this.http.get(ApiUrls.notification).pipe(
      map((resp: any) => {
        return resp.data.map(
          (value: any) => new Notification(
            value.id,
            value.title,
            value.message,
            value.relevance,
            new Date(value.creation)
          )
        );
      })
    );
  }

  newNotification(notif: Notification): Observable<any> {
    return this.http.post(ApiUrls.notification, notif);
  }
}
