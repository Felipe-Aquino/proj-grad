import { Component, OnInit } from '@angular/core';

import { environment } from '../../../../environments/environment';

import { parseString } from 'browser-xml2js';
// const parseString = require('browser-xml2js').parseString;

import { UserService } from '../services/user.service';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { File, FileInfo } from '../models/file.model';
import { TranslationService } from '../../../helpers/translation.service';

import { notify } from '../../ui/ui-toast.component';

@Component({
  selector: 'app-user-add-doc',
  templateUrl: './user-add-doc.component.html',
  styleUrls: ['./user-add-doc.component.css']
})
export class UserAddDocComponent implements OnInit {
  uploader!: FileUploader;
  fileForms: NewFileForm[];

  languages: Language[];

  useLattes: boolean;
  lattesFiles: LattesFile[];

  translations: any;

  constructor(private service: UserService, private translationService: TranslationService) {

    let tr = this.translationService.translate.bind(this.translationService);
    this.fileForms = [];

    this.translations = {
      name:     tr("general-name"),
      lang:     tr("general-language"),
      doc_name: tr("user-doc-name"),
      add_doc:  tr("user-add-document"),
      size:     tr("user-size"),
      progress: tr("user-progress"),
      choose:   tr("user-choose-file"),
      sugg_sep: tr("user-suggest-sep-keywords"),
      submit:   tr("general-submit"),
      notify_failed:  tr("user-notify-create-file-fail"),
      notify_created: tr("user-notify-file-created"),
      manually: tr("user-manually"),
      use_lattes: tr("user-using-lattes"),
      keywords: tr("user-keywords"),
      load_lattes: tr("user-load-lattes"),
    };

    this.languages = [
      { abbrev: 'en', name: tr("language-en") },
      { abbrev: 'es', name: tr("language-es") },
      { abbrev: 'pt', name: tr("language-pt") }
    ];

    this.useLattes = false;
    this.lattesFiles = [];

  }

  ngOnInit() {
    const token = localStorage.getItem('token');
    // const url = 'http://localhost:3333/user/file/upload?token=' + token;
    const url = `${environment.api}/user/file/upload?token=${token}`;

    this.uploader = new FileUploader({ url });
    this.uploader.onCompleteItem = (i, r, s, h) => this.onCompleteItem(i,r,s,h);
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    };
    this.uploader.onAfterAddingAll = (items: any) => {
      items.forEach((item: any) => {
        this.fileForms.push({
          name: '',
          keywords: '',
          lang: this.languages[0].abbrev,
          item
        });
      })
    };

  }

  addFile(filename: string, indexFileForm: number) {
    const { name, keywords, lang, item } = this.fileForms[indexFileForm];
    const givenKeywords = keywords.split(/\s*,\s*/i)
                                  .filter(value => value !== '');

    const fileInfo = new FileInfo(name, filename, lang, givenKeywords);

    this.service.createFile(fileInfo).subscribe(
      resp => {
        notify(this.translations.notify_created, 'success', 5000);

        this.fileForms.splice(indexFileForm, 1);
        item.remove();
      },
      err => {
        notify(this.translations.notify_failed, 'danger', 5000);
      })
  }

  uploadFile() {
    console.log('ooo', this.fileForms);
    for (let form of this.fileForms) {
      if (form.lattes && this.useLattes) {
        form.name = form.lattes.name;
        form.keywords = form.lattes.keywords.join(',');
      }

      const { name, keywords, item } = form;
      if (name != '' && keywords != '' && item != undefined) {
        item.upload();
      }
    }
  }

  onCompleteItem(item: any, response: any, status: any, headers: any) {
    let index = this.fileForms.findIndex(value => value.item == item);
    //console.log('onCompleteItem: form', item, this.fileForms, index);
    if (this.fileForms[index].lattes)
      this.addFileViaLattes(item.file.name, index);
    else
      this.addFile(item.file.name, index);
  }

  parseBytes(bytes: number) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

    if(bytes == 0 || bytes == 1)
      return `${bytes} Byte`;

    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];
  }

  setUseLattes(value: boolean) {
    this.useLattes = value;
    if (!this.useLattes) {
      for (let form of this.fileForms) {
        form.lattes = null;
      }
    }
    else {
      var _this = this;
      let onChange = function(event: any) {
        let reader = new FileReader();
        let target = event.target as any;

        reader.onload = function(evt: any) {
          if(evt.target.readyState != 2) return;
          if(evt.target.error) {
              alert('Error while reading file');
              return;
          }

          let xml: string = evt.target.result;
          parseString(xml, (err: any, result: any) => {
            if (!err) {
              try {
                let obj = result['CURRICULO-VITAE'];
                obj = obj['PRODUCAO-BIBLIOGRAFICA'][0];
                obj = obj['ARTIGOS-PUBLICADOS'][0];

                let articles: any = obj['ARTIGO-PUBLICADO'];

                for (let article of articles) {
                  let keywords: any = article['PALAVRAS-CHAVE'] ? article['PALAVRAS-CHAVE'][0].$ : {};
                  keywords = Object.values(keywords).filter(v => v != "");
                  let data = article['DADOS-BASICOS-DO-ARTIGO'][0].$;
                  let name = data['TITULO-DO-ARTIGO'];

                  _this.lattesFiles.push({ name, keywords });
                }

                console.log(_this.lattesFiles);
                //console.log('test lattes files', _this.lattesFiles);
                event.target.value = ""; // Clears the selected file
                //console.log('event', evt);
              } catch (error) {
                console.log('error', error);
                // @Refactor: translate this
                notify('Invalid XML!', 'warn', 5000);
              }
            } else {
               // @Refactor: translate this
               notify('Invalid XML!', 'warn', 5000);
            }
          });
        };

        //console.log('event**', event);
        reader.readAsText(target.files[0], 'ISO-8859-1');
      };

      setTimeout(() => {
        const element = document.getElementById('myfile')
        if (element) {
          element.onchange = (event: any) => onChange(event);
        }
      }, 150);
    }
  }

  lattesChanged(lattes: LattesFile, form: NewFileForm) {
    form.lattes = lattes;
  }

  addFileViaLattes(filename: string, indexFileForm: number) {
    const { lang, item } = this.fileForms[indexFileForm];
    const { name = '', keywords = [] } = this.fileForms[indexFileForm].lattes || {};

    const fileInfo = new FileInfo(name, filename, lang, keywords);

    this.service.createFile(fileInfo).subscribe(
      resp => {
        notify(this.translations.notify_created, 'success', 5000);

        this.fileForms.splice(indexFileForm, 1);
        item.remove();
      },
      err => {
        notify(this.translations.notify_failed, 'danger', 5000);
      })
  }
}

interface NewFileForm {
  name: string;
  keywords: string;
  lang: string;
  item: FileItem;
  lattes?: LattesFile | null;
}

interface Language {
  name: string;
  abbrev: string; // name abbreviation
}

interface LattesFile {
  name: string;
  keywords: string[];
}
