import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAddDocComponent } from './user-add-doc.component';

describe('UserAddDocComponent', () => {
  let component: UserAddDocComponent;
  let fixture: ComponentFixture<UserAddDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAddDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAddDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
