import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { TranslationService } from '../../../helpers/translation.service';
import { Notification } from '../models/notification.model';

import { notify } from '../../ui/ui-toast.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  title: string = '';
  message: string = '';
  relevanceLevel: number;

  translations: any;

  constructor(private service: UserService, private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.translations = {
      title:        tr("report-title"),
      title_holder: tr("report-placeholder-title"),
      message:      tr("report-message"),
      msg_holder:   tr("report-placeholder-message"),
      relevance:    tr("report-relevance"),
      rep_issue:    tr("report-an-issue"),
      low:          tr("report-relevance-type-low"),
      medium:       tr("report-relevance-type-medium"),
      high:         tr("report-relevance-type-high"),
      submit:       tr("general-submit"),
      notify_fail:  tr("report-notify-fail"),
      notify_success: tr("report-notify-success")
    };

    this.relevanceLevel = 0;
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log('submitting');
    let notif = new Notification(
      '',
      this.title,
      this.message,
      this.relevanceLevel,
      null
    );

    this.service.newNotification(notif).subscribe(
      r => {
        this.title = '';
        this.message = '';

        notify(this.translations.notify_success, 'success');
      },
      e => {
          notify(this.translations.notify_fail, 'danger');
      }
    );
  }

}
