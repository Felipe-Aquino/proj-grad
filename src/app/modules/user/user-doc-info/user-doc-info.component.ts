import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import { File } from '../models/file.model';
import { TranslationService } from '../../../helpers/translation.service';
import { CloudData, CloudOptions } from 'angular-tag-cloud-module';

@Component({
  selector: 'app-user-doc-info',
  templateUrl: './user-doc-info.component.html',
  styleUrls: ['./user-doc-info.component.css']
})
export class UserDocInfoComponent implements OnInit {
  wordData: CloudData[] = [];
  wordOpt: any = options;

  categories: any = [];

  showModal: boolean = false;

  @Output() showChange = new EventEmitter<boolean>();
  @Input() set show(value: boolean) {
    this.showModal = value;
  }

  _selected!: File;

  @Input()
  set selected(sel: File) {
    this._selected = sel;
    let data: CloudData[] = [];
    for (let kw of sel.keywords) {
      data.push({ text: kw.word, weight: kw.count });
    }

    this.wordData = data;

    let categs: any = {};

    let keywords: any[] = []
    keywords = keywords.concat(sel.keywords);
    keywords = keywords.concat(sel.given_keywords);

    for (let keyword of keywords) {
      if (keyword.classifications) {
        keyword.classifications.forEach((c: any) => {
          const label = c.label as string;
          categs[label] = categs[label] ? categs[label] + 1 : 1;
        });
      }
    }

    for (let c in categs) {
      this.categories.push({key: c, value: categs[c]});
    }

    this.categories.sort((a: any, b: any) => b.value - a.value);
    this.categories = this.categories.splice(0, 30);
    //console.log(this.categories.splice(0, 30));

    //setTimeout(() => this.word_cloud_chart.update());
  }

  translations: any;

  constructor(private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.showModal = false;

    this.translations = {
      for_:           tr("general-for"),
      info:           tr("user-doc-info"),
      gen_keywords:   tr("user-gen-keyword"),
      sug_keywords:   tr("user-suggested-keyword"),
      classification: tr("user-search-type-class"),
      created_in:     tr("user-created-in"),
    };
  }

  ngOnInit() {
  }

  dismiss() {
    this.showModal = false;
    this.showChange.emit(false);
  }

  selectedDate(): string {
    const lang = this.translationService.getLanguage();
    return this._selected.creation.toLocaleString(lang);
  }
}

let options = {
  settings: {
    minFontSize: 8,
    maxFontSize: 30,
  },
  margin: {
    top: 10,
    right: 10,
    bottom: 10,
    left: 10
  },
  labels: true // false to hide hover labels
};
