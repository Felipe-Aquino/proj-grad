import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDocInfoComponent } from './user-doc-info.component';

describe('UserDocInfoComponent', () => {
  let component: UserDocInfoComponent;
  let fixture: ComponentFixture<UserDocInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDocInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDocInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
