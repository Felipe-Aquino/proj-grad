export interface URI {
  label: string;
  uri: string;
};

export interface Keyword {
  word: string;
  count: number;
  translation: string;
  classifications: URI[];
};

export class File {
  constructor(
    public id: string,
    public name: string,
    public language: string,
    public keywords: Keyword[],
    public given_keywords: Keyword[],
    public public_: boolean,
    public creation: Date,
    public loading?: boolean
  ) { }
}

export class FileInfo {
  constructor(
    public name: string,
    public filename: string,
    public lang: string,
    public given_keywords: string[]
  ) { }
}
