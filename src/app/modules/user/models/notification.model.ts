export class Notification {
  constructor(
    public id: string,
    public title: string,
    public message: string,
    public relevance: number,
    public creation: Date | null
  ) { }
}
