class User {
  constructor(
    public name: string,
    public email: string,
    public password: string,
    public admin: boolean
  ) { }
}

export default User;
