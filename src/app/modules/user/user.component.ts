import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { AuthService } from '../login/services/auth.service';
import { File, FileInfo } from './models/file.model';
import { TranslationService } from '../../helpers/translation.service';

@Component({
  selector: 'user-root',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name: string = '';
  email: string = '';

  files: File[] = [];
  file_selected: File | null = null;
  selected!: number;
  items: string[] = [];
  showDocInfo: boolean = false;

  translations: any;

  constructor(
    private auth: AuthService,
    private translationService: TranslationService,
    private service: UserService
  ) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.service.getUsers().subscribe((resp: any) => {
      const user = resp.data[0];
      this.name = user.name;
      this.email = user.email;

      this.service.getFiles().subscribe((files: any) => {
        this.files = [];
        for(const file of files) {
          this.files.push(new File(
            file._id,
            file.name,
            file.language,
            file.keywords,
            file.given_keywords,
            file.public_,
            new Date(file.creation)
          ));
        }
      });
    });

    this.translations = {
      hello:     tr("general-hello"),
      greetings: tr("general-greetings"),
    };

    this.items = [
      tr("user-dashboard"),
      tr("user-add-file"),
      tr("user-your-files"),
      tr("general-notification"),
      tr("general-report"),
      tr("general-logout")
    ]
  }

  ngOnInit() {
    this.showDocInfo = false;
    this.file_selected = null;

    this.selected = 0;

  }

  selectMenu(i: number) {
    this.selected = i;
    if(this.selected == 5) {
      this.auth.logout();
    }
  }

  selectFile(file: File) {
    this.showDocInfo = true;
    this.file_selected = file;
  }

  deleteFile(id: string) {
    this.service.deleteFile(id).subscribe(() => {
      for(var i = 0; i < this.files.length; i++) {
        if(this.files[i].id == id) {
          this.files.splice(i, 1);
          break;
        }
      }
    });
  }

  updateFile(file: File) {
    this.service.editFile(file).subscribe(() => {
      console.log('updated');
    })
  }
}
