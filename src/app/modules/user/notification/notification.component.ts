import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Notification } from '../models/notification.model';
import { TranslationService } from '../../../helpers/translation.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  notifications: Notification[]; 

  translations: any;

  constructor(private service: UserService, private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.notifications = [];
    this.service.getNotifications().subscribe(
      resp => this.notifications = resp
    );
    
    this.translations = {
      your_notif: tr('notification-your-notifications'),
      mark:       tr('notification-mark-as-read'),
      sent_by:    tr('notification-sent-by-administrator'),
      no_notif:   tr('notification-not-available'),
      relevance:  tr('report-relevance'),
      low:        tr('report-relevance-type-low'),
      medium:     tr('report-relevance-type-medium'),
      high:       tr('report-relevance-type-high'),
    };
  }

  getCreationTime(notif: Notification) : string {
    let lang = this.translationService.getLanguage();
    if (notif.creation) {
      return notif.creation.toLocaleTimeString(lang);
    }

    return new Date().toLocaleTimeString(lang);
  }

  getCreationDate(notif: Notification) : string {
    let lang = this.translationService.getLanguage();
    if (notif.creation) {
      return notif.creation.toLocaleDateString(lang);
    }

    return new Date().toLocaleDateString(lang);
  }

  ngOnInit() {
  }

}
