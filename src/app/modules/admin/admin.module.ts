import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminService } from './services/admin.service';
import { AuthService } from '../login/services/auth.service';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminCustumersComponent } from './admin-custumers/admin-custumers.component';

import { SharedModule } from '../ui/ui-shared.module';
import { NotificationComponent } from './notification/notification.component';
import { ReportComponent } from './report/report.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AdminRoutingModule,
    SharedModule,
  ],
  declarations: [
    AdminComponent,
    AdminHomeComponent,
    AdminCustumersComponent,
    NotificationComponent,
    ReportComponent
  ],
  exports: [
    AdminHomeComponent,
  ],
  providers: [
    AdminService,
    AuthService
  ]
})
export class AdminModule { }
