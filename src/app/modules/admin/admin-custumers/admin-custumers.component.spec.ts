import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCustumersComponent } from './admin-custumers.component';

describe('AdminCustumersComponent', () => {
  let component: AdminCustumersComponent;
  let fixture: ComponentFixture<AdminCustumersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCustumersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCustumersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
