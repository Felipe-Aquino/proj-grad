import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import User from '../../user/models/user.model';
import { TranslationService } from '../../../helpers/translation.service';

@Component({
  selector: 'app-admin-custumers',
  templateUrl: './admin-custumers.component.html',
  styleUrls: ['./admin-custumers.component.css']
})
export class AdminCustumersComponent implements OnInit {
  @Input() users: User[] = [];
  @Output() onEdit: EventEmitter<User> = new EventEmitter<User>()
  @Output() onDelete: EventEmitter<User> = new EventEmitter<User>()

  translations: any;

  constructor(private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.translations = {
      del:   tr("general-delete"),
      name:  tr("general-name"),
      users: tr("general-users"),
    };
  }

  ngOnInit() {
  }

  changed(u: User) {
    this.onEdit.emit(u);
  }

  deleted(u: User) {
    this.onDelete.emit(u);
  }
}
