import { Component, OnInit } from '@angular/core';
import { AdminService } from './services/admin.service';
import { AuthService } from '../login/services/auth.service';
import User from '../user/models/user.model';
import { TranslationService } from '../../helpers/translation.service';

import { notify } from '../ui/ui-toast.component';

@Component({
  selector: 'admin-root',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  name: string = '';
  email: string = '';

  users: User[] = [];

  selected!: number;
  items: string[] = [];

  translations: any;

  constructor(
    private auth: AuthService,
    private translationService: TranslationService,
    private service: AdminService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.service.getUsers().subscribe(resp => {
      this.users = resp.data as User[];
      const adm = this.users.find(u => u.admin);

      if (adm) {
        this.name = adm.name;
        this.email = adm.email;
      } else {
        this.name = 'Admin';
        this.email = 'admin@admin.com'
      }
    });

    this.translations = {
      hello:     tr("general-hello"),
      greetings: tr("general-greetings"),
      user_del:  tr("admin-notify-user-deleted")
    };

    this.selected = 0;
    this.items = [
      tr("user-dashboard"),
      tr("admin-custumers"),
      tr("general-notification"),
      tr("general-report"),
      tr("general-logout")
    ]

  }

  ngOnInit() {
  }

  selectMenu(i: number) {
    this.selected = i;
    if(this.selected == 4) {
      this.auth.logout();
    }
  }

  userChanged(user: User) {
    this.service.editUser(user).subscribe(r => {
      console.log('user changed', r);
    })
  }

  userDeleted(user: User) {
    this.service.deleteUser(user).subscribe(r => {
      const idx = this.users.findIndex((value, index, obj) => value.email == user.email);
      if(idx > -1) {
        this.users.splice(idx, 1);
        notify(this.translations.user_del, 'success');
      }
    })
  }
}
