import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import 'rxjs/add/operator/map';

import User from '../../user/models/user.model';
import { File } from '../../user/models/file.model';
import { Notification } from '../models/notification.model';
import { ApiUrls } from '../../../config';

@Injectable()
export class AdminService {

  constructor(private http: HttpClient) {
  }

  createUser(user: User): Observable<any> {
    return this.http.post(ApiUrls.user, user);
  }

  getUsers(): Observable<any> {
    return this.http.get(ApiUrls.user);
  }

  editUser(user: User): Observable<any> {
    return this.http.put(ApiUrls.user, user);
  }

  deleteUser(user: User): Observable<any> {
    return this.http.post(ApiUrls.user_del, user);
  }

  countUsers(): Observable<any> {
    return this.http.get(ApiUrls.user_count).pipe(
      map((res: any) => res.data)
    );
  }

  countFiles(): Observable<any> {
    return this.http.get(ApiUrls.file_count).pipe(
      map((res: any) => res.data)
    );
  }

  getNotifications(): Observable<Notification[]> {
    return this.http.get(ApiUrls.notification).pipe(
      map((resp: any) => {
        return resp.data.map(
          (value: any) => new Notification(
            value.id,
            value.title,
            value.message,
            value.relevance,
            value.sender,
            new Date(value.creation)
          )
        );
      })
    );
  }

  newNotifications(notif: Notification, emails: string[]): Observable<any> {
    return this.http.post(ApiUrls.notification, { ...notif, emails });
  }
}
