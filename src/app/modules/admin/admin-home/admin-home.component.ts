import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { Notification } from '../models/notification.model';
import User from '../../user/models/user.model';
import { TranslationService } from '../../../helpers/translation.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  status: Status[];

  @Input() users: User[] = [];
  searched: string = '';
  filteredUsers: User[] = [];
  notifications: Notification[] = []; 
  translations: any;

  constructor(private service: AdminService, private translationService: TranslationService) {

    let tr = this.translationService.translate.bind(this.translationService);


    this.translations = {
      name:          tr("general-name"),
      user_search:   tr("admin-user-search"),
      notifications: tr("general-notification"),
      no_data:       tr("admin-no-data-found"),
      quick_search:  tr("admin-quick-search"),
      view_all:      tr("admin-view-all"),
    };

    this.status = [
      { amount: '0', info: tr("general-users"), icon: 'fa-user' },
      { amount: '0', info: tr("general-files"), icon: 'fa-file' },
      { amount: '0', info: this.translations.notifications, icon: 'fa-bell' }
    ];

    this.notifications = [];
    this.service.getNotifications().subscribe(
      resp => {
        this.notifications = resp.slice(0, 5);
        this.status[2].amount = resp.length.toString();
      }
    );

    this.searched = '';
    this.filteredUsers = [];
  }

  ngOnInit() {
    this.service.countUsers().subscribe(count => this.status[0].amount = count.toString());
    this.service.countFiles().subscribe(count => this.status[1].amount = count.toString());
  }

  onSearch(searched: string) {
    if (searched == '') {
      this.filteredUsers = [];
      return;
    }
    this.filteredUsers = this.users.filter(user => {
      const name = user.name.toLowerCase();
      const email = user.email.toLowerCase();

      return name.match(searched) || email.match(searched);
    });
  }
}

interface Status {
  info: string;
  amount: string;
  icon: string;
}
