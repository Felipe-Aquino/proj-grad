import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { TranslationService } from '../../../helpers/translation.service';
import { Notification } from '../models/notification.model';
import User from '../../user/models/user.model';

import { notify } from '../../ui/ui-toast.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  custumers: User[] = [];

  @Input()
  set users(users: User[]) {
    this.custumers = users.filter(user => !user.admin);
  }

  selectedEmails: string[] = [];
  selectedEmailsStr: string = '';
  title: string = '';
  message: string = '';
  relevanceLevel: number = 0;

  translations: any;

  constructor(private service: AdminService, private translationService: TranslationService) {
    let tr = this.translationService.translate.bind(this.translationService);

    this.translations = {
      users:        tr("general-users"),
      title:        tr("report-title"),
      title_holder: tr("report-placeholder-title"),
      message:      tr("report-message"),
      msg_holder:   tr("report-placeholder-message"),
      relevance:    tr("report-relevance"),
      rep_issue:    tr("report-an-user"),
      low:          tr("report-relevance-type-low"),
      medium:       tr("report-relevance-type-medium"),
      high:         tr("report-relevance-type-high"),
      submit:       tr("general-submit"),
      select:       tr("general-select"),
      select_all:   tr("general-select-all"),
      users_emails: tr("report-users-emails"),
      notify_fail:  tr("report-notify-fail"),
      notify_success: tr("report-notify-success")

    };

    this.relevanceLevel = 0;
    this.selectedEmails = [];
  }

  ngOnInit() {
    console.log(this.custumers);
  }

  onSubmit() {
    console.log('submitting');
    let notif = new Notification(
      '',
      this.title,
      this.message,
      this.relevanceLevel,
      null,
      null
    );

    if(this.selectedEmails.length > 0) {
      this.service.newNotifications(notif, this.selectedEmails).subscribe(
        r => {
          this.title = '';
          this.message = '';

          notify(this.translations.notify_success, 'success');
        },
        e => {
          notify(this.translations.notify_fail, 'danger');
        }
      );
    }
  }
  
  onUserSelect(selected: boolean[]) {
    let i = 0;
    this.selectedEmails = [];
    for (let user of this.custumers) {
      if (selected[i]) {
        this.selectedEmails.push(user.email);
      }
      i++;
    }
    this.selectedEmailsStr = this.selectedEmails.join(', ');
  }
}
