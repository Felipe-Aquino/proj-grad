export type Sender = { name: string, email: string };

export class Notification {
  constructor(
    public id: string,
    public title: string,
    public message: string,
    public relevance: number,
    public sender: Sender | null,
    public creation: Date | null
  ) { }
}
