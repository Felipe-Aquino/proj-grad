import { BrowserModule } from '@angular/platform-browser';
import { Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { TranslationService } from './helpers/translation.service';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './modules/ui/ui-shared.module';

import { AuthGuard } from './guards/auth.guard';
import { JwtInterceptor } from './helpers/jwt.interceptor';

import { AppComponent } from './app.component';
import { ToastComponent } from './modules/ui/ui-toast.component';

import { UserModule } from './modules/user/user.module';
import { LoginModule } from './modules/login/login.module';
import { RegisterModule } from './modules/register/register.module';
import { AdminModule } from './modules/admin/admin.module';

@NgModule({
  declarations: [
    AppComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    UserModule,
    LoginModule,
    RegisterModule,
    AdminModule,
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    TranslationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
