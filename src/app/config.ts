import { environment } from '../environments/environment';

const api = environment.api;
// const api = "http://localhost:3333";

export const ApiUrls = {
  api,
  auth:         `${api}/public/auth`,
  register:     `${api}/public/register`,
  user:         `${api}/user`,
  user_del:     `${api}/user/del`,
  user_count:   `${api}/user/count`,
  file:         `${api}/user/file`,
  file_search:  `${api}/user/file/search`,
  file_count:   `${api}/user/file/count`,
  notification: `${api}/user/notif`,
};
